from random import randint

name = (input("Hi! What is your name? ")).lower()
guess = 0
start_year = 1924
end_year = 2004
start_month = 1
end_month = 12
start_day = 1
end_day = 31
guess_month = randint(start_month,end_month)
guess_year = randint(start_year,end_year)
guess_day = randint(start_day, end_day)
possible_answers = ['yes','no', 'later', 'earlier']

months = {
    1: "January",
    2: "Febuary",
    3: "March",
    4: "April",
    5: "May",
    6: "June",
    7: "July",
    8: "August",
    9: "September",
    10: "October",
    11: "November",
    12: "December"
}

for i in range(4):
    guess += 1
    print("Guess ", guess,":" ,name, ", were you born in ", months[guess_month], "/", guess_day, "/", guess_year)
    check = False
    guess_month = randint(start_month, end_month)
    guess_year = randint(start_year, end_year)
    guess_day = randint(start_day, end_day)

    # Check input for validity and inproper chars
    while check is False:
        answer = input("yes, no, later, or earlier? \n")
        if answer in possible_answers:
            check = True
        else:
            continue
    
    if i == 3:
        print("I have other things to do. Good bye.") 
        break

    # Check to see if answer 
    if answer == 'yes':
        print("I knew it!")
        break
    elif answer == 'no':
        print("Drat! Lemme try again!")
        continue
    elif answer == 'later':
        print("Drat! Lemme try again!")
        start_year = guess_year
        continue
    elif answer == 'earlier':
        print("Drat! Lemme try again!")
        end_year = guess_year
        continue
    
    i += 1
